<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_joomclouds
 *
 * @copyright   Copyright (C) 2012 Asikart. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Generated by AKHelper - http://asikart.com
 */

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JoomcloudsHelper::_('include.core');



// Init some API objects
// ================================================================================
$app 	= JFactory::getApplication() ;
$date 	= JFactory::getDate( 'now' , JFactory::getConfig()->get('offset') ) ;
$doc 	= JFactory::getDocument();
$uri 	= JFactory::getURI() ;
$user	= JFactory::getUser();
$userId	= $user->get('id');



// List Control
// ================================================================================
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$originalOrders = array();
$sortFields = $this->getSortFields();

// For Joomla!3.0
// ================================================================================
if( JVERSION >= 3 ) {
	JHtml::_('bootstrap.tooltip');
	JHtml::_('dropdown.init');
	JHtml::_('formbehavior.chosen', 'select');
	
	// For Site
	if($app->isSite()) {
		JoomcloudsHelper::_('include.isis');
	}
}else{
	
	// For Site
	//AKHelper::_('include.addCss', 'buttons/delicious-buttons/delicious-buttons.css', 'ww');
    //AKHelper::_('include.addCSS', 'uikit/uikit.css');
    //AKHelper::_('include.addCSS', 'uikit/uikit.almost-flat.css');
    AKHelper::_('include.addCSS', 'bootstrap3/bootstrap.css');
    AKHelper::_('include.addCSS', 'progress-bar/progress-bar.css');
	JoomcloudsHelper::_('include.fixBootstrapToJoomla');
	
}



?>


<!-- Sort Table by Filter seletor -->
<script type="text/javascript">
Joomla.orderTable = function() {
	table = document.getElementById("sortTable");
	direction = document.getElementById("directionTable");
	order = table.options[table.selectedIndex].value;
	if (order != '<?php echo $listOrder; ?>') {
		dirn = 'asc';
	} else {
		dirn = direction.options[direction.selectedIndex].value;
	}
	
	Joomla.tableOrdering(order, dirn, '');
}
</script>

<?php include 'navbar.php' ;?>

<div id="Joomclouds" class="windwalker installs tablelist <?php echo (JVERSION >= 3) ? 'joomla30' : 'joomla25' ?>">
	
	<!-- Form Begin -->
	<form action="<?php echo JFactory::getURI()->toString(); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
		<?php if(!empty( $this->sidebar) && $app->isAdmin()): ?>
			
			<!-- Sidebar -->
			<div id="j-sidebar-container" class="span2">
				<h4 class="page-header"><?php echo JText::_('JOPTION_MENUS'); ?></h4>
				<?php echo $this->sidebar; ?>
				
				<hr />
				<!-- Filter Order -->
				<div class="">
					<select name="sortTable" id="sortTable" title="<?php echo JText::_('JGLOBAL_SORT_BY');?>" class=" span12 hasTooltip hasTip input-medium form-control" onchange="Joomla.orderTable()">
						<option></option>
						<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
					</select>
				</div>
				<hr />
				<!-- Filter Order Dir -->
				<div class="">
					<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
					<select name="directionTable" id="directionTable" class="input-medium form-control span12" onchange="Joomla.orderTable()">
						<option></option>
						<option value=""><?php echo JText::_('COM_JOOMCLOUDS_ORDER_DIR');?></option>
						<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('COM_JOOMCLOUDS_ORDER_DIR_ASC');?></option>
						<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('COM_JOOMCLOUDS_ORDER_DIR_DESC');?></option>
					</select>
				</div>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif;?>
		
		
		<div class="width-60 fltlft span7">
		<!-- Filter-->
		<?php echo $this->loadTemplate('filter'); ?>
	
		
		<!-- Table -->
		
			<div id="extension-box">
				<fieldset class="adminformlist">
					<?php echo $this->loadTemplate('table') ; ?>
				</fieldset>
			</div>
		</div>
		
		
		
		<!-- Install Box -->
		<div id="install-box" class="width-40 fltrt span5">
		
				<!-- Hidden Inputs -->
				<div id="hidden-inputs">
					<input type="hidden" name="task" value="" />
					<input type="hidden" name="boxchecked" value="0" />
					<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
					<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
					<input type="hidden" name="original_order_values" value="<?php echo implode($originalOrders, ','); ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</div>
	</form>
	
<?php echo $this->loadTemplate('login'); ?>
</div>