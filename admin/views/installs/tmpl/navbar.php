<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_joomclouds
 *
 * @copyright   Copyright (C) 2012 Asikart. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Generated by AKHelper - http://asikart.com
 */

// no direct access
defined('_JEXEC') or die;

$input  = JFactory::getApplication()->input ;
$layout = $input->get('layout', 'default');

JCCHelper::_('include.addCSS', 'glyphicons/css/bootstrap-glyphicons.css');

?>
<div id="main-navbar" class="navbar navbar-inverse">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a href="http://www.joomclouds.com" id="joomclouds-logo" class="navbar-brand brand" target="_blank" style="float: left;">
				<img src="components/com_joomclouds/images/joomclouds_logo_white_horz.png" alt="JoomClouds">
			</a>
			
            <?php if( $this->isLogin ): ?>
			<ul class="nav navbar-nav">
				<li class="<?php echo $layout == 'default' ? 'active' : ''; ?>">
					<a href="index.php?option=com_joomclouds">
						<?php echo JText::_('COM_JOOMCLOUDS_COLLECTIONS'); ?>
					</a>
				</li>
				<li class="<?php echo $layout == 'bundles' ? 'active' : ''; ?>">
					<a href="index.php?option=com_joomclouds&layout=bundles">
						<?php echo JText::_('COM_JOOMCLOUDS_BUNDLES'); ?>
					</a>
				</li>
				<li class="">
					<a href="http://www.joomclouds.com/my-admin/collections.html" target="_blank">
						<?php echo JText::_('COM_JOOMCLOUDS_MANAGE_COLLECTIONS'); ?>
						<span class="glyphicon glyphicon-share"></span>
					</a>
				</li>
			</ul>
            <?php endif; ?>
			
			<ul class="nav navbar-nav pull-right">
				<li>
					<?php echo $this->login_button; ?>
				</li>
			</ul>
		</div>
	</div>
</div>