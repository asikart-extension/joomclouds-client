/*!
 * com_joomclouds
 *
 * Copyright 2012 Asikart.com
 * License GNU General Public License version 2 or later; see LICENSE.txt, see LICENSE.php
 *
 * Generator: AKHelper
 * Author: Asika
 */

var Joomclouds = {
	installExtension : function(id, filetype, btn) {
		var e = new Element( 'fieldset',{
					'class' : 'adminformlist install-box-msg',
					html : '<div align="center" style="margin-top: 20px;"><img src="components/com_joomclouds/images/loading/loading-bar.gif" alt="Installing"></div>'
				} );
		
		var myRequest = new Request({
			url: 'index.php?option=com_joomclouds&task=install.install&tmpl=component&id=' + id + '&filetype=' + filetype,
			onProgress: function(event, xhr){
				//var loaded = event.loaded, total = event.total;
		 
				//console.log(parseInt(loaded / total * 100, 10));
			},
			onRequest: function(){
				btn.set('disabled', 'true');
				btn.addClass('disabled');
				
				var b = $$('#install-box') ;
				e.inject( b[0], 'top' ) ;
				
				e.highlight();
			},
			onSuccess: function(responseText){
				var result = JSON.decode(responseText);
                
                e.set('html', '<div class="alert alert-warning">'+result.AKResult.install_message+'</div><div class="extension-message">'+result.AKResult.extension_message+'</div>');
				
			}
		});
		 
		myRequest.send();
	}
    ,
    installBundle : function(id, collections, btn){
        collections.each(function(e){
            Joomclouds.installExtension(e.id, e.filetype, btn);
        });
    }
}

var JoomClouds = Joomclouds;