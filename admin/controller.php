<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_joomclouds
 *
 * @copyright   Copyright (C) 2012 Asikart. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Generated by AKHelper - http://asikart.com
 */

// no direct access
defined('_JEXEC') or die;

include_once AKPATH_COMPONENT.'/controller.php' ;

class JoomcloudsController extends AKController
{
	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Load the submenu.
		JoomcloudsHelper::addSubmenu(JRequest::getCmd('view', 'installs'));

		$view = JRequest::getCmd('view', 'installs');
        JRequest::setVar('view', $view);

		parent::display();
        
        // Debug
        if(AKDEBUG):
            echo '<hr style="clear:both;" />';
            echo AKHelper::_('system.renderProfiler', 'WindWalker') ;   
        endif;
        
		return $this;
	}
}
