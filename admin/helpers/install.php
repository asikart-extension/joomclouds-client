<?php
/**
 * @package     AKHelper
 * @subpackage  main
 *
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


// No direct access
defined('_JEXEC') or die;


class JoomcloudsHelperInstall
{
    
    
    /**
     * function downloadFromServer
     */
    public static function downloadFromServer($url, $path)
    {
        if(!$url) return ;
        
        jimport('joomla.filesystem.file');
        jimport('joomla.filesystem.folder');
        jimport('joomla.filesystem.path');
        
        $url    = JFactory::getURI($url) ;
        $path   = JPath::clean($path) ;
        
        //$folder_path = JPATH_ROOT.DS.'files'.DS.$url->task_id ;
        if(substr($path, -1) == DIRECTORY_SEPARATOR){
            $file_name      = JFile::getName($url);
            $file_path      = $path.$file_name ;
            $folder_path    = $path ;
        }else{
            $file_path      = $path ;
            $folder_path    = str_replace( JFile::getName($path), '', $file_path );
        }
        
        JPath::setPermissions($folder_path, 644, 755) ;
        if( !JFolder::exists($folder_path) ){
            JFolder::create($folder_path) ;
        }
        
        $fp = fopen( $file_path, 'w+' ) ;
        $ch = curl_init(  );
        
        $options = array(
            CURLOPT_URL             => AKHelper::_('uri.safe' ,$url),
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_USERAGENT       => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1",
            CURLOPT_FOLLOWLOCATION => !ini_get('open_basedir') ? true : false ,
            CURLOPT_FILE            => $fp ,
            CURLOPT_SSL_VERIFYPEER  => false
        );
        
        // Merge option
        foreach( $option as $key => $opt ):
            if( isset($option[$key]) ) $options[$key] = $option[$key] ;
        endforeach;
        
        curl_setopt_array($ch, $options);
        curl_exec($ch);
        
        $errno  = curl_errno($ch);
        $errmsg = curl_error($ch);
        
        curl_close($ch);
        fclose($fp);
        
        if( $errno ) {
            self::$errors[] = $errno . ' - ' . $errmsg  ;
            return false ;
        }else{
            return true ;
        }
    }
    
    
    
}