<?php
/**
 * @package     AKHelper
 * @subpackage  main
 *
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


// No direct access
defined('_JEXEC') or die;


class JoomcloudsHelperTablelist
{
    /**
     * function buildLabel
     */
    public static function buildLabels($collections, $extensions)
    {
        if( !count((array) $collections) ) {
            return '';
        }
        
        $label = array();
        foreach( $collections as $collection ):
            $label_type = self::isInstalled($extensions, $collection->element, $collection->folder) ? 'success' : 'default';
            $label[]    = "<span class=\"collections label label-{$label_type}\">{$collection->title}</span>";
        endforeach;
        
        return implode(' ', $label) ;
    }
    
    /**
     * function isInstalled
     */
    public static function isInstalled($extensions, $element, $folder)
    {
        $installed 	= false ;
		$element 	= $folder ? $element.'.'.$folder : $element ;
		
		if( isset($extensions[$element]) ):
			return true ;
		endif;
        
        return false ;
    }
	
	/**
	 * function type
	 */
	public static function types($type)
	{
		$typeMapping = array(
			'component' => 'success' ,
			'module' 	=> 'danger label-important' ,
			'plugin' 	=> 'info' ,
			'language' 	=> 'warning' ,
			'package'	=> 'warning' ,
			'template'	=> 'info' ,
			'file'		=> 'default'
		);
		
		$typetxt = strtoupper($type[0]) ;
		$typetip = JText::_('COM_JOOMCLOUDS_EXTENSION_TYPE_'.$type);
		
		$label_type = JArrayHelper::getValue($typeMapping, $type, 'default');
		
		$tip = JVERSION >= 3 ? 'hasTooltip' : 'hasTip' ;
		
		$label = "<span class=\"type-{$type} label label-{$label_type} {$tip}\" title=\"{$typetip}\">{$typetxt}</span>";
		
		return $label;
	}
	
	/**
	 * function environment
	 */
	public static function environments($environments)
	{
		$envMapping = array(
			'2.5' => 'warning' ,
			'3.x' => 'info'
		);
		
		$labels = array();
		
		foreach( $environments as $environment ):
			if(!$environment) continue ;
			
			$label = JArrayHelper::getValue( $envMapping, $environment, 'default' );
			$class = str_replace('.', '', $environment);
			
			$labels[] = "<span class=\"environment-{$class} label label-{$label}\">{$environment}</span>";
		endforeach;
		
		return implode(' ', $labels);
	}
}